#!/usr/bin/env bash
# Installer script for DotCastle.
#
DOTCASTLE="$(dirname $BASH_SOURCE)"

# Grab some helper functions for the script.
# These functions are only aimed at providing a nice looking interface on CLI.
source "${DOTCASTLE}/helpers/functions.sh"

# What are we working upon?
header "Welcome" "Attempting DotCastle Installation."

# ============================================================================ #
#                       VERIFY DEPENDENCIES
# ============================================================================ #
check_dependency "Git"    "git"
check_dependency "Python" "python"

# ============================================================================ #
#                       CONFIGURABLE OPTIONS
# ============================================================================ #

# # Allow the user to specify a custom location for the DotCastle repository.
# if ! confirm "Install DotCastle in ~/.dotcastle?" Y; then
#     ask "Where should I install DotCastle, then?"
#     location=$REPLY
# else
#     location="${HOME}/.dotcastle"
# fi

# # Allow the user to specify a custom DotCastle repository to import.
# if ! confirm "Import DotCastle repo: https://github.com/nikhgupta/dotfiles ?" Y; then
#     ask "Please, provide me the URL to a custom DotCastle repository, then."
#     repourl=$REPLY
# else
#     repourl="https://github.com/nikhgupta/dotfiles"
# fi

# ============================================================================ #
#                       DotCastle Repository
# ============================================================================ #

# # Clone the DotCastle repository to this machine at the specified location.
# if [ -e "${location}" ]; then
#     echo
#     say "Info" "Installation path is not empty!"
#     say "Ignoring.."
#     # error "Please, delete directory: $location to proceed." 1 "Failed"
# else
#     run "git clone ${repourl} ${location}" "Cloned castle repository."
# fi

# ============================================================================ #
#                       ANSIBLE INSTALLATION
# ============================================================================ #

# Allow user to change the location of Ansible's hosts file via Environ variable
[ -z "${ANSIBLE_HOSTS}" ] && export ANSIBLE_HOSTS="${DOTCASTLE}/ansible/hosts"
# Ensure that the hosts file has options to connect to the local machine.
[ -f "${ANSIBLE_HOSTS}" ] || echo "localhost  ansible_connection=local" > $ANSIBLE_HOSTS

# Install ansible, if not already installed on this system.
# We assume that Python is already installed on the system,
# and install Pip, if required.
if which ansible &>/dev/null; then
    echo
    say "Success" "`ansible --version` is already installed."
else
    run "sudo easy_install pip" "Installed Pip."
    run "sudo pip install markupsafe" "Installed ansible dependencies."
    cflags="CFLAGS='-Wunused-command-line-argument-hard-error-in-future'"
    run "sudo ${cflags} pip install ansible" "Installed `ansible --version`"
fi

# Verify that the ansible installation works as expected.
if ansible all -m ping 1>/dev/null; then
    say "Success" "Ansible's connection to the local machine has been verified."
else
    error "Could not verify ansible's connection to the local machine." 1 "Failed"
fi

# ============================================================================ #
#                       ANSIBLE PLAYBOOKS
# ============================================================================ #

echo
say "Info" "Running common ansible playbook."
ansible-playbook "${DOTCASTLE}/ansible/common.yml"


# Find the ansible playbook suitable for this machine.
kernel="`uname -v`"
[[ "$kernel" ==  "Darwin"* ]] && kernel=osx
[[ "$kernel" == *"Ubuntu"* ]] && kernel=ubuntu

if [ -z "${kernel}" ]; then
    echo
    say "Warning" "Unidentified system."
    say "Info"    "No system specific playbook was run."
elif [ -s "${DOTCASTLE}/ansible/${kernel}.yml" ]; then
    echo
    say "Info" "Running ansible playbook for kernel: ${kernel}."
    ansible-playbook "${DOTCASTLE}/ansible/${kernel}.yml"
fi
