DotCastle
=========

DotCastle is my dotfiles configuration, and helps me setup my development
machine with minimal efforts.

With DotCastle, I get the following:
- My preferred shell  and related configuration: ZSH
- My preferred editor and related configuration: VIM
- My preferred development machine setup, using: ANSIBLE

So, effectively, when I am on a new machine, I can setup my development
environment on that machine, by running the following set of commands:

```bash
# assume Ubuntu
sudo apt-get install git
git clone https://github.com/nikhgupta/dotcastle ~/.dotcastle
cd ~/.dotcastle
./install.sh
```
