#!/usr/bin/env bash

say() {
    status="            "
    case $1 in
        Info)           status_color="\e[36m" ;;
        Success|Pass*)  status_color="\e[32m" ;;
        Welcome|Warn*)  status_color="\e[33m" ;;
        Error|Fail*)    status_color="\e[31m" ;;
        Command)        status_color="\e[35m" ;;
        *)              status_color="\e[39m" ;;
    esac
    printf "${status_color}%s %s $2\e[39m\n" "$1" "${status:${#1}}"
}
rule() {
    delimiter="${1:-=}"
    printf "${delimiter}%.0s" {1..12}
    printf "  "
    printf "${delimiter}%.0s" {1..64}
    printf '\n'
}
header() {
    rule; say "${1}" "${2}"; rule
}
confirm() {
    while true; do
        if [ "${2:-}" = "Y" ]; then
            prompt="Y/n"
            default=Y
        elif [ "${2:-}" = "N" ]; then
            prompt="y/N"
            default=N
        else
            prompt="y/n"
            default=
        fi
 
        # Ask the question
        read -p "Question      $1 [$prompt] " -n 1 -r REPLY
 
        # Default?
        if [ -z "$REPLY" ]; then
            REPLY=$default
        else
            echo
        fi
 
        # Check if the reply is valid
        case "$REPLY" in
            Y*|y*) return 0 ;;
            N*|n*) return 1 ;;
        esac
    done
}
ask() {
    while true; do
        echo "Question      ${1}"
        read -ep "Answer?       " REPLY
        if [ -n "$REPLY" ]; then return 0; fi
    done
}
run() {
    echo
    command=$1
    say "Command" "${command}"

    if [ -n "$DEBUG" ]; then
        command="${command} 2>&1"
        rule "."
        eval $command
        code=$?
        rule "."
    else
        say "Info" "Running shell command. This might take a while.."
        command="${command} 2>&1 &>/dev/null"
        eval $command
        code=$?
    fi

    if (( $code )); then
        error "${3:-Command execution failed.}" $code
    else
        say "Success" "${2:-Command was executed successfully.}"
    fi

    return $code
}
error(){
    say "${3:-Error}" "$1"
    [ -z "$DEBUG" ] && say "Info" "Run with DEBUG environment variable set to get more info."
    exit $2
}

check_dependency() {
    name=$1
    binary=$2
    if which $binary &>/dev/null; then
        say "Passed" "Dependency met: ${name}"
    else
        say "Info"  "$name is a required dependency."
        say "Error" "A suitable version of $name was not found on this system."
        say "Task"  "Please, install $name before running this script."
        exit 1
    fi
}
